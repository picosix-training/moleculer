const { ServiceBroker } = require("moleculer");

describe("Hello world!", () => {
  let broker;

  beforeAll(async () => {
    broker = new ServiceBroker({ logger: process.env.NODE_ENV === 'development' });
    broker.createService({
      name: "moleculer",
      actions: {
        async say() {
          return "Hello world!";
        }
      }
    });
    await broker.start();
  });

  it('should return "Hello world!"', async () => {
    const response = await broker.call("moleculer.say");

    expect(response).toBe("Hello world!")
  });
});
